using System;
using System.Collections.Generic;

namespace FlockingBackend{

     ///<summary>
    ///This is a abstract to represent a Bird type object
    ///</summary>
    public abstract class Bird{

        ///<summary>
        ///This a field containing a float value of Rotation
       ///</summary>
           public float Rotation
        {
            get 
            {
                return (float)Math.Atan2(this.Velocity.Vy, this.Velocity.Vx); 
            }
        }
        ///<summary>
        ///Method incharge of making bird appear on the opposite side
       ///</summary>
        private void AppearOnOppositeSide()
       {
           if (this.Position.Vx > World.Width)
            {
                this.Position = new Vector2(0, this.Position.Vy);
            }
            else if(this.Position.Vx < 0)
            {
                 this.Position = new Vector2(World.Width, this.Position.Vy);
            }
            if (this.Position.Vy > World.Height)
            {
                this.Position = new Vector2(this.Position.Vx, 0);
            }
            else if(this.Position.Vy < 0)
            {
                this.Position= new Vector2(this.Position.Vx, World.Height);
            }
       }

       ///<summary>
        ///This a field containing containing the Position value of the Vector2
       ///</summary>
        public Vector2 Position{get; protected set;}
         ///<summary>
        ///This a field containing containing the Velocity value of the Vector2
       ///</summary>
        public Vector2 Velocity{get; protected set;}
         ///<summary>
        ///This a protected field containing containing the amountToSteer value of the Vector2
       ///</summary>
        protected Vector2 amountToSteer;

         ///<summary>
        ///Bird constructor that sets random values to Position,Velocity and sets amountToSteer to (0,0)
       ///</summary>
        public Bird(){
            Random rnd = new Random();

            Position = new Vector2(rnd.Next(World.Width),rnd.Next(World.Height));
            
            Velocity = new Vector2(rnd.Next(-4, 4),rnd.Next(-4, 4));

            amountToSteer = new Vector2(0,0);
  
        }

         ///<summary>
        ///Bird constructor  used in TestCases to set position and velocity to specific values
       ///</summary>
        public Bird(int postionX, int postionY, int velocityX, int velocityY){

            Position = new Vector2(postionX,postionY);
            
            Velocity = new Vector2(velocityX,velocityY);

            amountToSteer = new Vector2(0,0);       
        }

         ///<summary>
        ///Abstract Method that calculates the bahavior of the bird corresponding to the sparrowList
       ///</summary>

        public abstract void CalculateBehaviour(List<Sparrow> sparrowList);
         ///<summary>
        ///Method in charge of moving the bird
       ///</summary>
        public virtual void Move(){
            
            Velocity = new Vector2(this.Velocity.Vx + amountToSteer.Vx,this.Velocity.Vy + amountToSteer.Vy);
            
            Position = new Vector2(this.Position.Vx + this.Velocity.Vx,this.Position.Vy + this.Velocity.Vy);
            
            
            AppearOnOppositeSide();

        }



    }
}