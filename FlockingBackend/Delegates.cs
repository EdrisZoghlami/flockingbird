using System;
using System.Collections.Generic;


namespace FlockingBackend{

         ///<summary>
        ///Delegates used to raise the events to calculate the movement vector for each sparrow and raven
       ///</summary>
    
        public delegate void CalculateMoveVector(List<Sparrow> sparrowList);
        public delegate void MoveBird();
        public delegate void CalculateRavenAvoidance(Raven raven);
    
}