using System;
using System.Collections.Generic;

namespace FlockingBackend
{
    ///<summary>
    ///This class is the subscriber class that each bird subscribes to. The class also raises the events to calculate movement vector and move the birds.
    ///</summary>
    public class Flock
    { 
        ///<summary>
        ///This event is in charge of the birds placement
        ///</summary>
        public event CalculateMoveVector CalcMovementEvent;
        ///<summary>
        ///This event is in charge of the sparrows avoiding raven
        ///</summary>
        public event CalculateRavenAvoidance  CalcRavenFleeEvent;
        ///<summary>
        ///This event is in charge of moving the bird
        ///</summary>
        public event MoveBird  MoveEvent;
          ///<summary>
        ///Method subscribes event to delegate instances
        ///</summary>
        public void Subscribe(CalculateMoveVector calculateMoveVector, MoveBird moveBird, CalculateRavenAvoidance ravenAvoidance = null)
        {
            CalcMovementEvent += calculateMoveVector;
            MoveEvent += moveBird;
            CalcRavenFleeEvent += ravenAvoidance;
            
        }

        ///<summary>
        ///This method raises the calculate and move events
        ///</summary>
        ///<param name="sparrows">List of Sparrow objects</param>
        ///<param name="raven">A Raven object</param>
        public void RaiseMoveEvents(List<Sparrow> sparrows, Raven raven)
        {
            CalcMovementEvent?.Invoke(sparrows);
            CalcRavenFleeEvent?.Invoke(raven);
            MoveEvent?.Invoke();
        }

        
    }
}