using System;
using System.Collections.Generic;

namespace FlockingBackend
{
    ///<summary>
    ///This class is used to represent a single raven. 
    ///</summary>
    public class Raven: Bird
    { 
        ///<summary>
        ///Raven constructor that uses base constructor from Bird class
       ///</summary>
        public Raven() : base()
        {

        }
        ///<summary>
        ///Raven constructor that uses base constructor with parameters (used for test cases) from Bird class
       ///</summary>

        public Raven(int postionX, int postionY, int velocityX, int velocityY): base(postionX,postionY,velocityX,velocityY) 
        {
            
        }

        ///<summary>
        ///Method ovverides CalculateBehavior from Bird class and sets the amountToSteer variable to the method call of ChaseSparrow() 
       ///</summary>

       
        public override void CalculateBehaviour(List<Sparrow> sparrows) 
        {

            amountToSteer = ChaseSparrow(sparrows);
        }
        ///<summary>
        ///Method incharge of making bird appear on the opposite side
       ///</summary>

        private void AppearOnOppositeSide()
       {
           if (this.Position.Vx > World.Width)
            {
                this.Position = new Vector2(0, this.Position.Vy);
            }
            else if(this.Position.Vx < 0)
            {
                 this.Position = new Vector2(World.Width, this.Position.Vy);
            }
            if (this.Position.Vy > World.Height)
            {
                this.Position = new Vector2(this.Position.Vx, 0);
            }
            else if(this.Position.Vy < 0)
            {
                this.Position= new Vector2(this.Position.Vx, World.Height);
            }
       }
          ///<summary>
        ///Method incharge of the raven Algorithm
       ///</summary>

        public Vector2 ChaseSparrow (List<Sparrow> sparrows)
        {
            Sparrow closestSparrow = null;
            
            
            int closeEnoughSparrow = 0;
            Vector2 result;

            for (int i = 0; i < sparrows.Count; i++)
            {

                if(Vector2.DistanceSquared(this.Position,sparrows[i].Position)<(World.AvoidanceRadius * World.AvoidanceRadius)){
                    closeEnoughSparrow++;
                    closestSparrow=sparrows[i];
                }
                
            }
            if(closeEnoughSparrow==0){
                return new Vector2(0,0);
            }
            else{
                result = closestSparrow.Position - this.Position;
                return result;
            }
               
        }

         ///<summary>
        ///Method in charge of moving the raven
       ///</summary>

        public override void Move(){
            
            Velocity = new Vector2(this.Velocity.Vx + amountToSteer.Vx,this.Velocity.Vy + amountToSteer.Vy);
            Velocity = Vector2.Normalize(Velocity);
            Velocity = Velocity * World.MaxSpeed;
            
            Position = new Vector2(this.Position.Vx + this.Velocity.Vx,this.Position.Vy + this.Velocity.Vy);
            
            
            
            AppearOnOppositeSide();

        }   
                
    }
        
}
