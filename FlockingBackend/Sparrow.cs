using System;
using System.Collections.Generic;

namespace FlockingBackend
{
    ///<summary>
    ///This class is used to represent a single sparrow. 
    ///</summary>
    public class Sparrow : Bird
    {     
          ///<summary>
          ///This is a base constructor 
          ///</summary>
        public Sparrow() : base()
        {

        }

         ///<summary>
          ///This is constructor used for test cases
          ///</summary>

        public Sparrow(int postionX, int postionY, int velocityX, int velocityY): base(postionX,postionY,velocityX,velocityY) 
        {
            
        }


        ///<summary>
        ///This method is an event handler to calculate and set amountToSteer vector using the flocking algorithm rules
        ///</summary>
        ///<param name="sparrows">List of sparrows</param>
        public override void CalculateBehaviour(List<Sparrow> sparrows) 
        {
            amountToSteer =Cohesion(sparrows) + Alignment(sparrows) + Avoidance(sparrows);

        }
        ///<summary>
        ///This method is an event handler to calculate and update amountToSteer vector with the amount to steer to flee a chasing raven
        ///</summary>
        ///<param name="raven">A Raven object</param>
        public void CalculateRavenAvoidance(Raven raven)
        {
             amountToSteer = amountToSteer+FleeRaven(raven);
        }

         ///<summary>
        ///This method returns a Vector2 object and is part of the Alignment algorithm
        ///</summary>
        ///<param name="sparrows">A list of Sparrow</param>
        public Vector2 Alignment (List<Sparrow> sparrows){
            
            Vector2 result = new Vector2(0,0);
            int neighbourCount =  0;
            
            for (int i = 0; i < sparrows.Count; i++)
            {
                

                
                float distance = Vector2.DistanceSquared(this.Position,sparrows[i].Position);
                if(distance<(World.NeighbourRadius*World.NeighbourRadius) && this != sparrows[i]){
                    neighbourCount++;
                    result =  result +sparrows[i].Velocity;

                }
                
            }
            if(neighbourCount==0){
                return new Vector2(0,0);
            }
            else{
                result= result/neighbourCount;
                result = Vector2.Normalize(result);
                result = result * World.MaxSpeed;
                result = result - this.Velocity;
                result = Vector2.Normalize(result);
                return result;

            }
            
            
            
        
        }
         ///<summary>
        ///This method returns a Vector2 object and is part of the Cohesion algorithm
        ///</summary>
        ///<param name="sparrows">A list of Sparrow</param>
        public Vector2 Cohesion (List<Sparrow> sparrows){
            

            Vector2 result = new Vector2(0,0);
            int neighbourCount =  0;

            for (int i = 0; i < sparrows.Count; i++)
            {
               
                float distance = Vector2.DistanceSquared(this.Position,sparrows[i].Position);
                if(distance<(World.NeighbourRadius*World.NeighbourRadius) && this != sparrows[i]){
                    neighbourCount++;
                    result =  result +sparrows[i].Position;

                }
                
                
            }
            if(neighbourCount==0){
                return new Vector2(0,0);
            }
            else{
                result= result/neighbourCount; 
                result = result - this.Position;
                result = Vector2.Normalize(result);
                result = result * World.MaxSpeed;
                result = result - this.Velocity;
                result = Vector2.Normalize(result);
                return result;
            }
        }
         ///<summary>
        ///This method returns a Vector2 object and is part of the Avoidance algorithm
        ///</summary>
        ///<param name="sparrows">A list of Sparrow</param>
        public Vector2 Avoidance (List<Sparrow> sparrows){
            

            Vector2 result = new Vector2(0,0);
            int neighbourCount =  0;
            

            for (int i = 0; i < sparrows.Count; i++)
            {
                
                float distance = Vector2.DistanceSquared(this.Position,sparrows[i].Position);
                if(distance<(World.AvoidanceRadius*World.AvoidanceRadius) && this != sparrows[i]){
                    Vector2 positionDifference = this.Position - sparrows[i].Position;
                    
                    positionDifference= positionDifference/distance;
                    result =  result + positionDifference;
                    neighbourCount++;  
                }
                
                
            }
             if(neighbourCount==0){
                return new Vector2(0,0);
            }
            else{
                result= result/neighbourCount;
                result = Vector2.Normalize(result);
                result = result * World.MaxSpeed;
                result = result - this.Velocity;
                result = Vector2.Normalize(result);
                return result;

            }

        }
         ///<summary>
        ///This method returns a Vector2 object and is part of the FleeRaven algorithm
        ///</summary>
        ///<param name="Raven">A Raven Object</param>
        public Vector2 FleeRaven(Raven raven){
            

            float distance = Vector2.DistanceSquared(this.Position,raven.Position);
             if(distance<(World.AvoidanceRadius*World.AvoidanceRadius)){
                Vector2 positionDifference = this.Position - raven.Position;
                positionDifference= positionDifference/distance;
                positionDifference = Vector2.Normalize(positionDifference);
                positionDifference = positionDifference * World.MaxSpeed;
                return positionDifference;
                 
             }

             else{
                 
             return new Vector2 (0,0);
             }
        }
        
      
    }
}