using System;
using System.Collections.Generic;

namespace FlockingBackend
{
    ///<summary>
        ///This is a struct to represent a Vector2
    ///</summary>
        
    public struct Vector2
    {   
        ///<summary>
        ///This a field containing the x value of the Vector
       ///</summary>
        public float Vx { get; set; }
         ///<summary>
        ///This a field containing the y value of the Vector
       ///</summary>
        public float Vy { get; set; }

         ///<summary>
        ///This a constructor that sets the Vector 2 object
       ///</summary>
        ///<param name="x">A float represent x value</param>
         ///<param name="y">A float representing y value</param>
        public Vector2(float x, float y)
        {
            Vx = x;
            Vy = y;
        }
        ///<summary>
        ///Overriding the + operator to add 2 vectors together
       ///</summary>
        public static Vector2 operator +(Vector2 a, Vector2 b)
        {
            float newX = a.Vx + b.Vx;
            float newY = a.Vy + b.Vy;
            return new Vector2(newX,newY);
        }
         ///<summary>
        ///Overriding the - operator to substract 2 vectors together
       ///</summary>

        public static Vector2 operator -(Vector2 a, Vector2 b)
        {
            float newX = a.Vx - b.Vx;
            float newY = a.Vy - b.Vy;
            return new Vector2(newX,newY);
        }
         ///<summary>
        ///Overriding the / operator to divide a vector by a scalar.
       ///</summary>

        public static Vector2 operator /(Vector2 a, float scalar)
        {
            
            float newX = a.Vx/scalar;
            float newY = a.Vy/scalar;
            return new Vector2(newX,newY);
        }

         ///<summary>
        ///Overriding the * operator to multiply a vector by a scalar.
       ///</summary>
         public static Vector2 operator *(Vector2 a, float scalar)
        {
            float newX = a.Vx * scalar;
            float newY = a.Vy * scalar;
            return new Vector2(newX,newY);
        }
        ///<summary>
        ///Method returns a float representing the distance squared of 2 vectors.
       ///</summary>
        public static float DistanceSquared(Vector2 a, Vector2 b)
        {
            float distance = (float) (Math.Pow(a.Vx-b.Vx,2) + Math.Pow(a.Vy-b.Vy,2));
            
           
            return distance;
        }
         ///<summary>
        ///Method returns a Vector2 representing the normalized vector.
       ///</summary>
        public static Vector2 Normalize(Vector2 a)
        {
            float magnitude = (float) (Math.Sqrt((a.Vx*a.Vx) + (a.Vy*a.Vy)));
            float newX = a.Vx /magnitude;
            float newY = a.Vy/magnitude;
            return new Vector2(newX,newY);
        }

        
        
    }

}