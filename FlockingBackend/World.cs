using System;
using System.Collections.Generic;

namespace FlockingBackend
{
    ///<summary>
    ///This class represents the world of our 2D birds
    ///</summary>
    public class World
    {
        ///<summary>
        ///Objects fields, and public static fields 
        ///</summary>
        private Flock _flock;
        public Raven raven;

        public List<Sparrow> sparrows{ get;}

        public static  int InitialCount {get;}
        public static  int Width {get;}
        public static  int Height {get;}
        public static  int MaxSpeed {get;}
        public static  int NeighbourRadius {get;}
        public static  int AvoidanceRadius {get;}
    
        ///<summary>
        ///This is a static World constructor that sets the basic values of the world object.
        ///</summary>
        static World(){
            InitialCount = 150;
            Width = 1000; 
            Height = 500;
            MaxSpeed = 4;
            NeighbourRadius = 100;
            AvoidanceRadius = 50;
            
        }

        ///<summary>
        ///Empty Constructor that initializes the flock object, sparrows list and raven object and subscribes the sparrows and raven to the flock object.
        ///</summary>

        public World(){
            _flock = new Flock();
            sparrows = new List<Sparrow>();
            raven = new Raven();
            for (int i = 0; i < InitialCount; i++)
            {
                sparrows.Add(new Sparrow());
                _flock.Subscribe(sparrows[i].CalculateBehaviour,sparrows[i].Move,sparrows[i].CalculateRavenAvoidance);
            }
            _flock.Subscribe(raven.CalculateBehaviour,raven.Move);
        }
        ///<summary>
        ///Method raises events
        ///</summary>
        public void Update(){

            this._flock.RaiseMoveEvents(sparrows,this.raven);

        }
    }
    
}