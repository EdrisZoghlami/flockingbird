﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using FlockingBackend;
using System;
using System.Collections.Generic;

namespace FlockingSimulation
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager graphics;
        private SpriteBatch _spriteBatch;
        public World world;

        private SparrowFlockSprite sparrowFlockSprite;

        private RavenSprite ravenSprite;
        

        public Game1()
        {
            
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            this.world = new World();
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            
            this.sparrowFlockSprite = new SparrowFlockSprite(this);
            Components.Add(sparrowFlockSprite);
            this.ravenSprite = new RavenSprite(this);
            Components.Add(ravenSprite);
            graphics.PreferredBackBufferHeight = World.Height;
            graphics.PreferredBackBufferWidth = World.Width;
            graphics.ApplyChanges();
            

            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            world.Update();

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
