using FlockingBackend;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Media;
using System.Diagnostics;
using System;



namespace FlockingSimulation
{
    public class RavenSprite : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        private Game1 game;
        private Texture2D ravenTexture;

        private Raven raven;

        public RavenSprite(Game1 game) : base(game)
        {
            
            this.game = game;
            
        }

                // TODO: Add your initialization logic here  
        public override void Initialize()
        { 
            this.raven = game.world.raven;
            base.Initialize();
        }

        // TODO: use this.Content to load your game content here
        protected override void LoadContent()
        {
            
            spriteBatch = new SpriteBatch(GraphicsDevice);
            this.ravenTexture = game.Content.Load<Texture2D>("raven");
            
            base.LoadContent();
        }


        // TODO: Add your update logic here 
        public override void Update(GameTime gameTime)
        {    
          base.Update(gameTime);
        }


        // TODO: Add your drawing code here 
        public override void Draw(GameTime gameTime)
        { 
 
            spriteBatch.Begin();

            

            spriteBatch.Draw(ravenTexture, new Microsoft.Xna.Framework.Vector2(raven.Position.Vx, raven.Position.Vy), null, Color.White, raven.Rotation, new Microsoft.Xna.Framework.Vector2(10, 10), 1, SpriteEffects.None, 0f);
            
            spriteBatch.End();
            base.Draw(gameTime);
;
        }

    }
}