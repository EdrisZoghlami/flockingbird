using FlockingBackend;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Media;
using System.Diagnostics;
using System;



namespace FlockingSimulation
{
    public class SparrowFlockSprite : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        private Game1 game;
        private Texture2D sparrowTexture;
        List<Sparrow> sparrowList;

        public SparrowFlockSprite(Game1 game) : base(game)
        {
            
            this.game = game;
            
        }

                // TODO: Add your initialization logic here  
        public override void Initialize()
        { 
            this.sparrowList = game.world.sparrows;
            base.Initialize();
        }

        // TODO: use this.Content to load your game content here
        protected override void LoadContent()
        {
            
            spriteBatch = new SpriteBatch(GraphicsDevice);
            this.sparrowTexture = game.Content.Load<Texture2D>("sparrow");
            
            base.LoadContent();
        }


        // TODO: Add your update logic here 
        public override void Update(GameTime gameTime)
        {    
          base.Update(gameTime);
        }


        // TODO: Add your drawing code here 
        public override void Draw(GameTime gameTime)
        { 
 
            spriteBatch.Begin();

            foreach (var sparrow in this.sparrowList){ 

                spriteBatch.Draw(sparrowTexture, new Microsoft.Xna.Framework.Vector2(sparrow.Position.Vx, sparrow.Position.Vy), null, Color.White, sparrow.Rotation, new Microsoft.Xna.Framework.Vector2(10, 10), 1, SpriteEffects.None, 0f);
            }
            spriteBatch.End();
            base.Draw(gameTime);
;
        }


    }
}