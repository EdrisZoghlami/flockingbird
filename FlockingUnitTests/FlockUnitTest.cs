using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using FlockingBackend;
using System;

namespace FlockingUnitTests
{
    [TestClass]
    public class FlockUnitTest
    {
        [TestMethod]
        public void TestRaiseMoveEvents()
        {
            //Arrange
            Flock f = new Flock();

            List<Sparrow> spList = new List<Sparrow>();
            spList.Add(new Sparrow(3,6,7,6));
            spList.Add(new Sparrow(4,7,7,8));
            spList.Add(new Sparrow(5,8,7,8));

            Raven r = new Raven(2,4,6,8);
            for (int i = 0; i < spList.Count; i++)
            {
                f.Subscribe(spList[i].CalculateBehaviour,spList[i].Move,spList[i].CalculateRavenAvoidance);
            }
            f.Subscribe(r.CalculateBehaviour,r.Move);
            f.RaiseMoveEvents(spList,r);

            //Act
            Vector2 result = r.ChaseSparrow(spList);

            //Assert
            
            
        }
    }
}
