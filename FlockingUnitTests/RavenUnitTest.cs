using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using FlockingBackend;
using System;

namespace FlockingUnitTests
{
    [TestClass]
    public class RavenUnitTest
    {
        [TestMethod]
        public void TestChaseSparrow()
        {
            //Arrange

            List<Sparrow> spList = new List<Sparrow>();
            spList.Add(new Sparrow(3,6,7,6));
            spList.Add(new Sparrow(4,7,7,8));
            spList.Add(new Sparrow(5,8,7,8));

            Raven r = new Raven(2,4,6,8);

            float expX =3;
            float expY = 4;

            //Act
            Vector2 result = r.ChaseSparrow(spList);

            //Assert
            Assert.AreEqual(expX,result.Vx,0.01);
            Assert.AreEqual(expY,result.Vy,0.01);
            
        }
    }
}
