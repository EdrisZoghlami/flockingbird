using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using FlockingBackend;
using System;

namespace FlockingUnitTests
{
    [TestClass]
    public class SparrowUnitTest
    {
        [TestMethod]
        public void TestAlignment()
        {
            //Arrange

            List<Sparrow> spList = new List<Sparrow>();
            spList.Add(new Sparrow(3,6,7,6));
            spList.Add(new Sparrow(4,7,7,8));
            spList.Add(new Sparrow(5,8,7,8));

            Sparrow s = new Sparrow(2,4,6,8);

            float expX = -0.54f;
            float expY = -0.84f;

            //Act
            Vector2 result = s.Alignment(spList);

            //Assert
            Assert.AreEqual(expX,result.Vx,0.01);
            Assert.AreEqual(expY,result.Vy,0.01);
            
        }

          [TestMethod]
          public void TestCohesion()
        {
            //Arrange

            List<Sparrow> spList = new List<Sparrow>();
            spList.Add(new Sparrow(3,6,7,6));
            spList.Add(new Sparrow(4,7,7,8));
            spList.Add(new Sparrow(5,8,7,8));

            Sparrow s = new Sparrow(2,4,6,8);

            float expX = -0.63f;
            float expY = -0.78f;

            //Act
            Vector2 result = s.Cohesion(spList);

            //Assert
            Assert.AreEqual(expX,result.Vx,0.01);
            Assert.AreEqual(expY,result.Vy,0.01);
            
        }

           [TestMethod]
          public void TestAvoidance()
        {
            //Arrange

            List<Sparrow> spList = new List<Sparrow>();
            spList.Add(new Sparrow(3,6,7,6));
            spList.Add(new Sparrow(4,7,7,8));
            spList.Add(new Sparrow(5,8,7,8));

            Sparrow s = new Sparrow(2,4,6,8);

            float expX = -0.58f;
            float expY = -0.82f;

            //Act
            Vector2 result = s.Avoidance(spList);

            //Assert
            Assert.AreEqual(expX,result.Vx,0.01);
            Assert.AreEqual(expY,result.Vy,0.01);
            
        }

            [TestMethod]
          public void FleeRaven()
        {
            //Arrange

            List<Sparrow> spList = new List<Sparrow>();
            spList.Add(new Sparrow(3,6,7,6));
            spList.Add(new Sparrow(4,7,7,8));
            spList.Add(new Sparrow(5,8,7,8));

            Sparrow s = new Sparrow(2,4,6,8);
            Raven r = new Raven(3,6,9,10);

            float expX = -1.79f;
            float expY = -3.58f;

            //Act
            Vector2 result = s.FleeRaven(r);

            //Assert
            Assert.AreEqual(expX,result.Vx,0.01);
            Assert.AreEqual(expY,result.Vy,0.01);
            
        }
    }
}
