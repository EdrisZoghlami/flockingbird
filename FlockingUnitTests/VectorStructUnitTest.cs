using FlockingBackend;
using Microsoft.VisualStudio.TestTools.UnitTesting;




namespace FlockingUnitTests
{
    [TestClass]
    public class VectorStructUnitTest
    {
        [TestMethod]
        public void TestVectorAddition()
        {
            //Arrange
            float x = 3;
            float y = 4;

            float t = 5;
            float v = 6;



            //Act
            Vector2 a = new Vector2(x,y);
            Vector2 b = new Vector2(t,v);
            Vector2 result = new Vector2(8,10);

            //Assert
            Assert.AreEqual(a+b,result);



        }
        [TestMethod]
        public void TestVectorSubstraction()
        {
            //Arrange
            float x = 3;
            float y = 4;

            float t = 5;
            float v = 6;

            //Act
            Vector2 a = new Vector2(x,y);
            Vector2 b = new Vector2(t,v);
            Vector2 result = new Vector2(-2,-2);

            //Assert
            Assert.AreEqual(a-b,result);

        }

        [TestMethod]
        public void TestVectorDivision()
        {
            //Arrange
            float x = 4;
            float y = 6;

            //Act
            Vector2 a = new Vector2(x,y);
            Vector2 result = new Vector2(2,3);

            //Assert
            Assert.AreEqual(a/2,result);

        }

         [TestMethod]
        public void TestVectorDivisionZero()
        {
            //Arrange
            float x = 4;
            float y = 6;

            //Act
            Vector2 a = new Vector2(x,y);
            a = a/0;
            Vector2 result = new Vector2(0,0);

            //Assert
            AssertFailedException.Equals(a,result);

        }

        [TestMethod]
        public void TestVectorMultiplication()
        {
            //Arrange
            float x = 4;
            float y = 6;

            //Act
            Vector2 a = new Vector2(x,y);
            a = a * 3;
            Vector2 result = new Vector2(12,18);

            //Assert
            Assert.AreEqual(a,result);

        }

        
        [TestMethod]
        public void TestVectorDistanceSquared()
        {
            //Arrange
            float x = 4;
            float y = 6;

            float t = 8;
            float v = 10;

            //Act
            Vector2 a = new Vector2(x,y);
            Vector2 b = new Vector2(t,v);
           
            float result = 16 + 16;

            //Assert
            Assert.AreEqual(Vector2.DistanceSquared(a,b),result);

        }

        [TestMethod]
        public void TestVectorNormalization()
        {
            //Arrange
            float x = 9;
            float y = 0;

            //Act
            Vector2 a = new Vector2(x,y);
        
           Vector2 result = new Vector2(1,0);

            //Assert
            Assert.AreEqual(Vector2.Normalize(a),result);

        }

        




    }
}